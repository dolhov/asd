#include <stdio.h>
#include <math.h>


int main()
{
    float x = 0;
    float y = 0;
    float z = 0;
    float a = 0;
    float b = 0;
    double pi = 3.14159265358979323846;
    puts("Enter x, y, z: ");
    scanf("%f %f %f", &x, &y, &z);
    if (0 == x)
        puts("ERROR: invalid x");
    else if (0 == y)
        puts("ERROR: invalid y ");
    else if ( z + pow(y,2) < 0)
        puts("ERROR");
    else 
    {
        a =(pow(x, 3) * pow(z + pow(y,2), 1/2))/(3 * pow(y, pow(x, -z)+ 2)) + 1;
        float g = a;
        if (a == pi / 2)
            puts("ERROR");
        else
        {
            while (g > pi/2)
            {
                g -= pi;
            }
            while (g < pi/2)
            {
                g += pi;
            }
            if (g == pi/2)
                puts("ERROR");
            else
            {
                a = tan(a);
                if (a + pow(y, 3) == 0)
                    puts("ERROR");
                else if (a == 0)
                    puts("ERROR");
                else if ( pow(a, 2) - pow(y, 3) * sin(z) / pow(a, 4) < 0)
                    puts("ERROR");
                else
                { 
                    b = cos(z /(a + pow(y, 3))) + sqrt(( pow(a, 2) - pow(y, 3) * sin(z) / pow(a, 4)));
                    printf("a: %f, b: %f", a, b);
                }
            }
        }
    }
}