#include <stdio.h>
#include <math.h>

int numdays(int m, int year)
{
    int d = 0;
    int q = 0;
    int y = 0;
    int g = 0;
    if (m <= 5)
    {
        q = m % 2;
        if ( q == 1)
            return d = 31;
        else
            return d = 30;
    }
    else if (5 < m && m <= 10)
    {
        q = m % 2;
        if (q == 1)
            return d = 30;
        else
            return d = 31;
    }
    else if (m == 11)
        return d = 31;
    else 
    {
        y = year % 4;
        if ( y == 0)
        {
            g = year % 400;
            if (g == 0)
                return d = 29;
            else
            {
                g = year % 100;
                if (g == 0)
                    return d = 28;
                else
                    return d = 29;
            }
        }
        return d = 28;
    }
}


int main()
{
    int year = 0;
    puts("Enter year: ");
    scanf("%i", &year);
    int d = 0;
    int m = 11; 
    int t = 0;
    int c = (year / 100);
    int y = (year % 100);
    int c1 = ((year - 1) / 100);
    int y1 = ((year - 1) % 100);
    if (year < 1582 || year > 4902)
        puts("ERROR: invalid year");
    else
    {
        while (m <= 12)
        {
            d = numdays(m, year) - 6;
            while (t != 4)
            {
                int i = (2.6 * m - 0.2) ;
                t = (i + d + y1 + y1 / 4 + c1 / 4 - 2 * c1) % 7;
                if (t < 0)
                    t += 7;
                d += 1;
            }
            printf("Day: %i; Month: %i\n", (d - 1), m % 10);
            t = 0;
            m += 1;
        }
        m = 1;
        while ( m < 11)
        {
            d = numdays(m, year) - 6;
            while (t != 4)
            {
                int i = (2.6 * m - 0.2);
                t =  (i + d + y + y / 4 + c / 4 - 2 * c) % 7;
                if (t < 0)
                    t += 7;
                d += 1;
            }
            printf("Day: %i; Month: %i\n", d - 1, m + 2);
            t = 0;
            m += 1;
        }
    }
}