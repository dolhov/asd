#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#define red "\x1B[31m"
#define black "\x1B[0m"


int main()
{
    srand(time(0));
    size_t N = 0;
    printf("Enter size of an array: ");
    scanf("%zu", &N);
    printf("\n");
    int * buffer = malloc(N * sizeof(int));
    if (buffer == NULL)
    {
        exit(1);
    }
    int * firstArr = malloc(N * sizeof(int));
    if (firstArr == NULL)
    {
        exit(1);
    }
    for (int i = 0; i < N; i++)
    {
        buffer[i] = rand() % (200) + 10;
    }
    for (int i = 0; i < N; i++)
    {
        firstArr[i] = buffer[i];
    }
    for (int i = 0; i < N - 1; i++)
    {
        if (buffer[i] / 100 > 0 && buffer[i] / 100 < 10)
        {
            int new_max = i;
            for (int q = i + 1; q < N; q++)
            {
                if ((buffer[q] > buffer[new_max]) && (buffer[q] / 100 > 0) && buffer[q] / 100 < 10)
                    new_max = q;
            }
            if (new_max != i)
            {
                int tmp = buffer[i];
                buffer[i] = buffer[new_max];
                buffer[new_max] = tmp;
            }

        }
        else if (buffer[i] / 100 == 0)
        {
            int new_min = i;
            for (int q = i + 1; q < N; q++)
            {
                if(buffer[q] < buffer[new_min] && buffer[q] / 100 == 0)
                    new_min = q;
            }
            if (new_min != i)
            {
                int tmp = buffer[i];
                buffer[i] = buffer[new_min];
                buffer[new_min] = tmp;
            }
        }
    }
    for (int i = 0; i < N; i++)
    {
        if (firstArr[i] == buffer[i])
        {
            printf(red "%i "black, firstArr[i]);
        }
        else
        {
            printf("%i ", firstArr[i]);
        }
    }
    printf("\n --- \n");
    for (int i = 0; i < N; i++)
    {
        if (buffer[i] == firstArr[i])
        {
            printf(red "%i "black, buffer[i]);
        }
        else
        {
            printf("%i ", buffer[i]);
        }
    }
    free(buffer);
    free(firstArr);
}