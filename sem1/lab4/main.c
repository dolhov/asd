#include <stdio.h>
#include <stdlib.h>




struct SLNode
{
    int data;
    struct SLNode * next;
};

struct DLNode
{
    int data;
    struct DLNode * prev;
    struct DLNode * next;
};
//пользовательские
struct DLNode * DLNode_findMid(struct DLNode * head);


//по заданию 
struct SLNode * slnode_create(int data);
struct DLNode * dlnode_create(int data);

struct DLNode * DLNode_add(struct DLNode * head, struct DLNode * node);
struct SLNode * SLNode_add(struct SLNode * head, struct SLNode * node);
struct DLNode * DLNode_add_byRules(struct DLNode * head, struct DLNode * node);

struct DLNode * DLNode_deleteThis(struct DLNode *head);
struct DLNode * DLNode_delete_byRules(struct DLNode *head);

void SLNode_print(struct SLNode * node);
void DLNode_print(struct DLNode * node);

struct SLNode * SLNode_clear(struct SLNode * head);
struct DLNode * DLNode_clear(struct DLNode * head);


unsigned int SLNode_size(struct SLNode * head);
unsigned int DLNode_size(struct DLNode * head);

struct SLNode * createSecondList(struct DLNode * head);

int main()
{
    struct DLNode * dlnode_head = NULL;
    struct SLNode * slnode_head = NULL;
    struct SLNode * second_list = NULL;
    while (1)
    {
        printf("1. Innitialise linked list\n");
        printf("2. Innitialise double linked list\n");
        printf("3. Print lists\n");
        printf("4. Task1\n");
        printf("5. Add node to slnode by task2\n");
        printf("6. Deinit all lists\n");
        printf("7. Double linked node delete by rules\n");
        printf("8. exit\n\n");
        int choise = -1;
        printf("Enter option number:");
        scanf("%i", &choise);
        puts("");
        if (choise == 1)
        {
            
            if (slnode_head == NULL)
            {
                int data = 0;
                printf("Enter data to node:");
                scanf("%i", &data);
                slnode_head = slnode_create(data);
                SLNode_print(slnode_head);
                puts("");
                printf("Size: %i", SLNode_size(slnode_head));
            }
            else
            {
                puts("You have already initialised linked list");
            }
            
        }
        else if (choise == 2)
        {
            if (dlnode_head == NULL)
            {
                int data = 0;
                printf("Enter data to node:");
                scanf("%i", &data);
                dlnode_head = dlnode_create(data);
                DLNode_print(dlnode_head);
                puts("");
                printf("Size: %i", DLNode_size(dlnode_head));
            }
            else
            {
                puts("You have already initialised double linked list");
            }

        }
        else if (choise == 3)
        {
            puts("Linked node:");
            SLNode_print(slnode_head);
            puts("");
            printf("Size: %i", SLNode_size(slnode_head));
            puts("Double linked node:");
            puts("");
            DLNode_print(dlnode_head);
            puts("");
            printf("Size: %i", DLNode_size(dlnode_head));
            puts("Second list:");
            SLNode_print(second_list);
            puts("");
            printf("Size: %i", SLNode_size(second_list));
        }
        else if (choise == 4)
        {
            if (dlnode_head != NULL)
            {
                int data = 0;
                printf("Enter data to node:");
                scanf("%i", &data);
                dlnode_head = DLNode_add_byRules(dlnode_head, dlnode_create(data));
                DLNode_print(dlnode_head);
                puts("");
                printf("Size: %i", DLNode_size(dlnode_head));
            }
            else
            {
                printf("ERROR: initialise double linked list first\n");
            }
        }
        else if (choise == 5)
        {
            if (slnode_head != NULL)
            {
                int data = 0;
                printf("Enter data to node:");
                scanf("%i", &data);
                slnode_head = SLNode_add(slnode_head, slnode_create(data));

                SLNode_print(slnode_head);
                puts("");
                printf("Size: %i", SLNode_size(slnode_head));
            }
            else
            {
                printf("ERROR: linked list first\n");
            }
        }
        else if (choise == 6)
        {
            if (slnode_head != NULL)
            {
                slnode_head = SLNode_clear(slnode_head);
            }
            if (dlnode_head != NULL)
            {
                dlnode_head = DLNode_clear(dlnode_head);
            }
            if (second_list != NULL)
            {
                SLNode_clear(second_list);
            }
            puts("SLNode");
            SLNode_print(slnode_head);
            puts("");
            printf("Size: %i", SLNode_size(slnode_head));
            puts("");
            puts("Double linked list");
            DLNode_print(dlnode_head);
            puts("");
            printf("Size: %i", DLNode_size(dlnode_head));
            puts("");
            puts("Second List");
            SLNode_print(second_list);
            puts("");
            printf("Size: %i", SLNode_size(second_list));
        }
        else if (choise == 7)
        {
            if (dlnode_head != NULL)
            {
                struct SLNode *toAdd = createSecondList(dlnode_head);
                struct SLNode *current = toAdd;
                while (current->next != NULL)
                {
                    current = current->next;
                }
                SLNode_add(second_list, current);
                second_list = toAdd;
                dlnode_head = DLNode_delete_byRules(dlnode_head);
                printf("Double linked node:\n");
                DLNode_print(dlnode_head);
                puts("");
                printf("Size: %i", DLNode_size(dlnode_head));
                printf("Second list:\n");
                SLNode_print(second_list);
                puts("");
                printf("Size: %i", SLNode_size(slnode_head));
            }
            else
            {
                printf("ERROR: initialise double linked list first\n");
            }
        }
        else if (choise == 8)
        {
            if (slnode_head != NULL)
            {
                SLNode_clear(slnode_head);
            }
            if (dlnode_head != NULL)
            {
                DLNode_clear(dlnode_head);
            }
            if (second_list != NULL)
            {
                SLNode_clear(second_list);
            }
            exit(0);
        }
        puts("\n\n");
    }
}


struct SLNode * slnode_create(int data)
{
    struct SLNode * pnode = malloc(sizeof(struct SLNode));
    pnode->data = data;
    pnode->next = NULL;
    return pnode;
}


struct DLNode * dlnode_create(int data)
{
    struct DLNode * pnode = malloc(sizeof(struct DLNode));
    pnode->data = data;
    pnode->next = NULL;
    pnode->prev = NULL;
    return pnode;
}

struct DLNode *DLNode_add_byRules(struct DLNode *head, struct DLNode *node)
{
    if (node != NULL)
    {
        struct DLNode *current = NULL;
        for (struct DLNode *counter = head; counter != NULL;)
        {
            if (counter->data > 0)
            {
                current = counter;
            }
            counter = counter->next;
        }
        if (current != NULL)
        {
            node->next = current->next;
            node->prev = current;
            if (current->next != NULL)
            {
                current->next->prev = node;
            }
            current->next = node;
            return head;
        }
        else
        {
            node->next = head;
            head->prev = node;
            head = node;
            return head;
        }
    }
    return head;
}


struct DLNode * DLNode_findMid(struct DLNode * head)
{
    int counter = 0;
    struct DLNode * current = head;
    while (current != NULL)
    {
        counter += 1;
        current = current->next;
    }
    int mid = counter / 2;
    counter = 0;
    current = head;
    while (mid > counter)
    {
        current = current->next;
        counter += 1;
    }
    return current;
}


struct DLNode * DLNode_deleteThis(struct DLNode * node)
{
    struct DLNode * tmp = node->next;
    if (node->next != NULL)
    {
        node->next->prev = node->prev;
    }
    if (node->prev != NULL)
    {
        node->prev->next = node->next;
    }
    free(node);
    return tmp;
}

struct SLNode * SLNode_add(struct SLNode * head, struct SLNode * node)
{
    if (node != NULL)
    {
        node->next = head;
        head = node;
    }
    return head;
}

struct DLNode * DLNode_add(struct DLNode * head, struct DLNode * node)
{
    if (node != NULL)
    {
        node->next = head;
        head->prev = node;
        head = node;
    }
    return head;
}

struct DLNode * DLNode_delete_byRules(struct DLNode *head)
{
    struct DLNode * mid = DLNode_findMid(head);
    struct DLNode * current = head;
    while (current != NULL)
    {
        if (current->data < mid->data)
        {
            if (current == head)
            {
                head = current->next;
            }
            current = DLNode_deleteThis(current);
        }
        else
        {
            current = current->next;
        }
    }
    return head;
}

void SLNode_print(struct SLNode * node)
{
    while (node != NULL)
    {
        printf("%i -> ", node->data);
        node = node->next;
    }
    printf("NULL\n");
}

void DLNode_print(struct DLNode * node)
{
    while (node != NULL)
    {
        printf("%i <-> ", node->data);
        node = node->next;
    }
    printf("NULL\n");
}

struct SLNode * SLNode_clear(struct SLNode * head)
{
    struct SLNode * node = head;
    while (node != NULL)
    {
        struct SLNode * next = node->next;
        free(node);
        node = next;
    }
    head = NULL;
    return head;
}

struct DLNode * DLNode_clear(struct DLNode * head)
{
    struct DLNode * node = head;
    while (node != NULL)
    {
        struct DLNode * next = node->next;
        free(node);
        node->next = NULL;
        node = next;
    }
    head = NULL;
    return head;
}

unsigned int SLNode_size(struct SLNode * head)
{
    unsigned int counter = 0;
    while (head != NULL)
    {
        counter += 1;
        head = head->next;
    }
    return counter;
}

unsigned int DLNode_size(struct DLNode * head)
{
    unsigned int counter = 0;
    while (head != NULL)
    {
        counter += 1;
        head = head->next;
    }
    return counter;
}


struct SLNode * createSecondList(struct DLNode * head)
{
    struct DLNode * mid = DLNode_findMid(head);
    struct DLNode * current = head;
    struct SLNode * slnode_head = NULL;
    while (current->next != NULL)
    {
        current = current->next;
    }
    while (current->prev != NULL)
    {
        if (current->data < mid->data)
        {
            slnode_head = SLNode_add(slnode_head, slnode_create(current->data));
        }
        current = current->prev;
    }
    if (current->data < mid->data)
    {
        slnode_head = SLNode_add(slnode_head, slnode_create(current->data));
    }
    return slnode_head;
}