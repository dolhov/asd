#include <stdio.h>
#include <stdlib.h>


int main()
{
    struct DLNode * dlnode_head = NULL;
    struct SLNode * slnode_head = NULL;
    struct SLNode * second_list = NULL;
    while (1)
    {
        printf("1. Innitialise linked list\n");
        printf("2. Innitialise double linked list\n");
        printf("3. Print lists\n");
        printf("4. Task1\n");
        printf("5. Add node to slnode by task2\n");
        printf("6. Deinit all lists\n");
        printf("7. Double linked node delete by rules\n");
        printf("8. exit\n\n");
        int choise = -1;
        printf("Enter option number:");
        scanf("%i", &choise);
        puts("");
        if (choise == 1)
        {
            
            if (slnode_head == NULL)
            {
                int data = 0;
                printf("Enter data to node:");
                scanf("%i", &data);
                slnode_head = slnode_create(data);
                SLNode_print(slnode_head);
            }
            else
            {
                puts("You have already initialised linked list");
            }
            
        }
        else if (choise == 2)
        {
            if (dlnode_head == NULL)
            {
                int data = 0;
                printf("Enter data to node:");
                scanf("%i", &data);
                dlnode_head = dlnode_create(data);
                DLNode_print(dlnode_head);
            }
            else
            {
                puts("You have already initialised double linked list");
            }

        }
        else if (choise == 3)
        {
            puts("Linked node:");
            SLNode_print(slnode_head);
            puts("Double linked node:");
            DLNode_print(dlnode_head);
            puts("Second list:");
            SLNode_print(second_list);
        }
        else if (choise == 4)
        {
            int data = 0;
            printf("Enter data to node:");
            scanf("%i", &data);
            dlnode_head = DLNode_add_byRules(dlnode_head, dlnode_create(data));
            DLNode_print(dlnode_head);
        }
        else if (choise == 5)
        {
            int data = 0;
            printf("Enter data to node:");
            scanf("%i", &data);
            slnode_head = SLNode_add(slnode_head, slnode_create(data));

            SLNode_print(slnode_head);
        }
        else if (choise == 6)
        {
            if (slnode_head != NULL)
            {
                slnode_head = SLNode_clear(slnode_head);
            }
            if (dlnode_head != NULL)
            {
                dlnode_head = DLNode_clear(dlnode_head);
            }
            if (second_list != NULL)
            {
                SLNode_clear(second_list);
            }
            puts("SLNode");
            SLNode_print(slnode_head);
            puts("Double linked list");
            DLNode_print(dlnode_head);
            puts("Second List");
            SLNode_print(slnode_head);
        }
        else if (choise == 7)
        {
            struct SLNode * toAdd = createSecondList(dlnode_head);
            struct SLNode * current = toAdd;
            while (current->next != NULL)
            {
                current = current->next;
            }
            SLNode_add(second_list, current);
            second_list = toAdd;
            dlnode_head = DLNode_delete_byRules(dlnode_head);
            printf("Double linked node:\n");
            DLNode_print(dlnode_head);
            printf("Second list:\n");
            SLNode_print(second_list);
        }
        else if (choise == 8)
        {
            if (slnode_head != NULL)
            {
                SLNode_clear(slnode_head);
            }
            if (dlnode_head != NULL)
            {
                DLNode_clear(dlnode_head);
            }
            if (second_list != NULL)
            {
                SLNode_clear(second_list);
            }
            exit(0);
        }
        puts("\n\n");
    }
}