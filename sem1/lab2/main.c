#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>


void MatrixPart (int x, int y, int N)
{
    printf("The index of the element is: %i:%i", x + 1, y + 1);
    printf("\n");
    if (x == y)
    {
        printf("The element is on the main diagonal");
    }
    else if (x > y)
    {
        printf("The element is above the main diagonal");
    }
    else if (x < y)
    {
        printf("The element is under the main diagonal");
    }
    printf("\n");
    if (x + y == N - 1)
    {
        printf("The element is on the secondary diagonal");
    }
    else if (x + y < N - 1)
    {
        printf("The element is above the secondary diagonal");
    }
    else if (x + y > N - 1)
    {
        printf("The element is under the secondary diagonal");
    }
    printf("\n");
}

int main()
{
    bool mybreak = false;
    int array[1000][1000] = {0};
    srand(time(0));
    int x = 0;
    int y = 0;
    int N = 0;
    int M = 0;
    int k = 0;
    int counter = 1;
    int command = 0;
    int rmax = 0;
    int rmin = 0;
    printf("Enter size of an array: ");
    scanf("%i %i", &N, &M);
    if (N != M || N % 2 == 0 || N <= 0) 
    {
        printf("ERROR: invalid array size input");
    }
    else
    {
        printf("Enter k: ");
        scanf("%i", &k);
        printf("\n");
        while (1 > 0)
        {
            printf("Do you want to randomize values in the array?\n1.Yes\n2.No\nEnter number of the option: ");
            scanf("%i", &command);
            if (command == 1)
            {
                printf("\nEnter min and max rand: ");
                scanf("%i %i", &rmin, &rmax);
                if (rmin > rmax)
                {
                    printf("ERROR: incorrect input: rand min can't be more than rand max\n\n");
                    continue;
                }
                for (int i = 0; i <= N; i++)
                {
                    for (int j = 0; j <= M; j++)
                    {
                        array[i][j] = rand()%(rmax - rmin + 1) + rmin;
                    }
                }
                break;
            }
            else if (command == 2)
            {
                int tmp = 0;
                for (int i = 0; i < M; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        array[i][j] = tmp;
                        tmp += 1;
                    }
                }
                break;
            }
            printf("ERROR: incorrect input\n\n");
        }
        x = N / 2; //
        y = x; // coordinates of the center of an array
        for (int i = 0; i < N; i++)
        {
            for (int j = 0; j < N; j++)
                printf("%i ", array[i][j]);
            printf("\n");
        }
        if (array[x][y] == k)
        {
            MatrixPart(x, y, N);
            mybreak = true;
        }
        printf("[%i] [%i] == %i\n", x+1, y+1, array[x][y]);
        while (mybreak != true)
        {
            for(int i = 0; i < counter; i++)
            {
                y += 1;
                if (array[x][y] == k)
                {
                    MatrixPart(x, y, N);
                    mybreak = true;
                    break;
                }
                printf("[%i] [%i] == %i\n", x+1, y+1, array[x][y]);
                if ( x == N - 1 && y == M - 1)
                {
                    mybreak = true;
                    printf("Not found\n");
                    break;
                }
            }
            if (mybreak == true)
                break;
            for(int i = 0; i < counter; i++)
            {
                x -= 1;
                if (array[x][y] == k)
                {
                    MatrixPart(x, y, N);
                    mybreak = true;
                    break;
                }
                printf("[%i] [%i] == %i\n", x+1, y+1, array[x][y]);
                if ( x == N - 1 && y == M - 1)
                {
                    mybreak = true;
                    printf("Not found\n");
                    break;
                }
            }
            counter += 1;
            if (mybreak == true)
                break;
            for(int i = 0; i < counter; i++)
            {
                y -= 1;
                if (array[x][y] == k)
                {
                    MatrixPart(x, y, N);
                    mybreak = true;
                    break;
                }
                printf("[%i] [%i] == %i\n", x+1, y+1, array[x][y]);
                if ( x == N - 1 && y == M - 1)
                {
                    mybreak = true;
                    printf("Not found\n");
                    break;
                }
            }
            if (mybreak == true)
                break;
            for(int i = 0; i < counter; i++)
            {
                x += 1;
                if (array[x][y] == k)
                {
                    MatrixPart(x, y, N);
                    mybreak = true;
                    break;
                }
                printf("[%i] [%i] == %i\n", x+1, y+1, array[x][y]);
                if ( x == N - 1 && y == M - 1)
                {
                    mybreak = true;
                    printf("Not found\n");
                    break;
                }
            }
            counter += 1;
            if (mybreak == true)
                break;
        }                                        
    }
}