#include <iostream>
#include "binaryheap.h"
using namespace std;

int main()
{
    int value = 0;
    BinaryHeap bin;
    while (true)
    {
        cin >> value;
        if (value == -1)
        {
            if (bin.size() != 0)
            {
                if (!bin.PreOrderTraversal()) // check is min binary heap
                {
                    cout << "\nTree is not min bin heap\n";
                    value = 0;
                    cout << "\nHeapify start\n";
                    bin.PostOrderTraversal(0); // heapify
                    cout << "\nHeapify end\n";

                    bin.printTree();
                }
                else
                {
                    cout << "Tree is min bin heap\n";
                }
                
            }
        }
        else
        {
            bin.insert(value);
        }
    }
}