#include <vector>
#include <iostream>
using namespace std;


class BinaryHeap
{
    vector<int> tree;
    int depth = 0;
public:
    void insert(int value)
    {
        tree.push_back(value);
        printTree();
    }

    int size()
    {
        return tree.size();
    }
    bool PreOrderTraversal(int i = 0, bool isMin = true) // Check if tree is MinBinHeap
    {
        if (i != 0 && tree[(i - 1) / 2] > tree[i])
        {
            return false;
        }
        if (2 * i + 1 < tree.size() && isMin == true)
        {
            isMin = PreOrderTraversal(2 * i + 1, isMin == true);
            if (2 * i + 2 < tree.size() - 1 && isMin == true)
            {
                isMin = PreOrderTraversal(2 * i + 2, true);
            }
        }
        return isMin;
    }
    void PostOrderTraversal(int i) // heapify
    {
        if (2 * i + 1 < tree.size())
        {
            PostOrderTraversal(2 * i + 1); // visit left subtree
        }
        if (2 * i + 2 < tree.size())
        {
            PostOrderTraversal(2 * i + 2); // visit right subtree
        }
        if (heapify(i))
        {
            this->printTree();
            std::cout << "\n";
        }
        
    }

    bool heapify(int i)
    {
        bool isChanged = false;
        int leftChild;
        int rightChild;
        int smallestChild;
        if (2 * i + 2 < tree.size())
        {
            leftChild = 2 * i + 1;
            rightChild = 2 * i + 2;
            smallestChild = i;

            if (tree[leftChild] < tree[smallestChild])
            {
                smallestChild = leftChild;
                isChanged = true;
            }

            if (tree[rightChild] < tree[smallestChild])
            {
                smallestChild = rightChild;
                isChanged = true;
            }

            if (smallestChild != i)
            {
                int temp = tree[i];
                tree[i] = tree[smallestChild];
                tree[smallestChild] = temp;
                i = smallestChild;
            }
        }
        if (2 * i + 1 < tree.size() && tree[i] > tree[2 * i + 1])
        {
            int temp = tree[i];
            tree[i] = tree[2 * i + 1];
            tree[2 * i + 1] = temp;
            isChanged = true;
        }
        return isChanged;
    }

    void printTree(int i = 0, char pos = '+', int depth = 0)
    {
        bool hasChild = false;
        if (2 * i + 1 < tree.size())
        {
            hasChild = true;
        }

        printValueOnLevel(i, pos, depth);
        if (hasChild)
        {
            printTree(2 * i + 1, 'L', depth + 1);
        }
        hasChild = false;
        if (2 * i + 2 < tree.size())
        {
            hasChild = true;
        }
        if (hasChild)
            printTree(2 * i + 2, 'R', depth + 1);
    }

    void printValueOnLevel(int i, char pos, int depth)
    {
        for (int j = 0; j < depth; j++)
        {
            printf("....");
        }
        std::cout << pos;
        if (i <= tree.size())
        {
            std::cout << "["  << tree[i] << "]\n";
        }
    }
};