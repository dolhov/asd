#include <iostream>
#include <string>

class CircledArrayQueue
{
    std::string * array_;
    size_t array_capacity_;
    size_t size_;
    int head_;
    int tail_;
public:
    CircledArrayQueue() {array_ = new std::string [6];
    array_capacity_ = 6;
    size_ = 0;
    head_ = 2;
    tail_ = 1;}
    ~CircledArrayQueue() {delete[] array_;}
    size_t size() {return size_;}
    int head() {return head_;}
    int tail() {return tail_;}
    void resize(int new_size) {
        if (new_size < array_capacity_) return;
        std::cout << "\nIT'S RESIZE TIME!!!!1111!!!11\n";
        std::cout << "IT'S RESIZE TIME!!!!1111!!!11\n";
        std::cout << "IT'S RESIZE TIME!!!!1111!!!11\n";
        std::cout << "Old size: " << size_ << " New size: " << new_size << "\n"; 
        std::string * new_array = new std::string [new_size];
        int j = head_;
        for (int i = new_size - (size_ - head_); i < new_size; i++)
        {
            new_array[i] = array_[j];
            j++;
        }
        if (tail_ < head_)
        {
            for (int i = 0; i <= tail_; i++)
            {
                new_array[i] = array_[i];
            }
        }
        else
        {
            tail_ = size_ - 1;
        }
        head_ = new_size - (size_ - head_);
        delete[] array_;
        array_capacity_ = new_size;
        array_ = new_array;
    }
    void enque(std::string str)
    {
        if (array_capacity_ == size_)
        {
            resize (size_ * 2);
        }
        if (tail_ != array_capacity_ - 1)
        {
            array_[tail_ + 1] = str;
            tail_++;
        }
        else
        {
            tail_ = 0;
            array_[tail_] = str;
        }
        size_++;
    }
    std::string deque()
    {
        if (size_ != 0)
        {
            std::string str = array_[head_];
            if (head_ != array_capacity_ - 1)
            {
                head_++;
            }
            else
            {
                head_ = 0;
            }
            size_--;
            return str;
        }
    }
    void print()
    {
        std::cout << "\n";
        if (size_ != 0)
        {
            if (tail_ < head_)
            {
                for (int i = head_; i < array_capacity_; i++)
                {
                    std::cout << array_[i] << " <-- ";
                }
                for (int i = 0; i <= tail_; i++)
                {
                    std::cout << array_[i] << " <-- ";
                }
            }
            else
            {
                for (int i = head_; i <= tail_; i++)
                {
                    std::cout << array_[i] << " <-- ";
                }
            }
        }
        else
        {
            std::cout << "The queue is empty";
        }
        std::cout << std::endl;
    }
};

int main()
{
    CircledArrayQueue queue;
    std::string entered_line;
    while(1)
    {
        std::getline(std::cin, entered_line);
        std::cout << "You entered: " << entered_line << std::endl;
        if (entered_line == "quit")
        {
            std::cout << "Program ends here\n";
            break;
        }
        if (entered_line != "out")
        {
            queue.enque(entered_line);
            queue.print();
        }
        else
        {
            queue.deque();
            queue.print();
        }
    }
}