#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#define red "\x1B[31m"
#define black "\x1B[0m"


int main()
{
    size_t K = 1;
    srand(time(0));
    size_t M = 0;
    size_t N = 0;
    printf("Enter size of an array. M: ");
    scanf("%zu", &M);
    printf("\n");
    printf("N: ");
    scanf("%zu", &N);
    printf("\n");
    printf("Enter divider K: ");
    scanf("%zu", &K);
    printf("\n");
    int my_array[M][K];
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
        {
            my_array[i][j] = rand() % (20) + 10;
        }
    }
    /* Выводим в консоль разукрашенный массив и паралельно находим наибольший элемент
    находим количество элементов которые нужно посортировать   */ 
    size_t max = 0;
    size_t quantity_of_elements = 0;
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if (my_array[i][j] % K == 0 || (my_array[i][j] % K) % 2 == 1)
            {
                printf(red "%i " black, my_array[i][j]);
                quantity_of_elements += 1;
                if (my_array[i][j] > max)
                {
                    max = my_array[i][j];
                }
            }
            else
            {
                printf("%i ", my_array[i][j]);
            }
        }
        printf("\n");
    }
    int array[quantity_of_elements];
    {
        int q = 0;
        for (int i = 0; i < M; i++)
        {
            for (int j = 0; j < N; j++)
            {
                if (my_array[i][j] % K == 0 || (my_array[i][j] % K) % 2 == 1)
                {
                    array[q] = my_array[i][j];
                    q++;
                }
            }
        }
    }

    int count[max + 1];
    for (int i = 0; i < max + 1; i++)
    {
        count[i] = 0;
    }
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if (my_array[i][j] % K == 0 || (my_array[i][j] % K) % 2 == 1)
            {
                count[my_array[i][j]] += 1;
            }
        }
    }
    for (int i = 1; i < max + 1; i++)
    {
        int b = count[i - 1];
        count[i] += b;
    }
    int sorted[quantity_of_elements];
    for (int i = quantity_of_elements - 1; i >= 0; i--)
    {
        sorted[count[array[i]] - 1] = array[i];
        count[array[i]] -= 1;
    }
    {
        int q = 0;
        for (int i = 0; i < M; i++)
        {
            for (int j = 0; j < N; j++)
            {
                if (my_array[i][j] % K == 0 || (my_array[i][j] % K) % 2 == 1)
                {
                    my_array[i][j] = sorted[q];
                    q += 1;
                }
            }
        }
    }
    printf("Sorted OD array: \n");
    for (int i = 0; i < quantity_of_elements; i++)
    {
        printf("%i ", sorted[i]);
    }
    puts("");
    printf("Sorted array: \n");
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
        {
            if (my_array[i][j] % K == 0 || (my_array[i][j] % K) % 2 == 1)
            {
                printf(red "%i " black, my_array[i][j]);
            }
            else
            {
                printf("%i ", my_array[i][j]);
            }
        }
        printf("\n");
    }
}