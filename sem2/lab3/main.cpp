#include <iostream>
#include "hashtable.h"
using namespace std;



int main()
{
    ticket tic;
    hashtable table;
    unsigned long long int key;
    int option = -1;
    while (1)
    {
        if (std::cin.fail())
        {
            // get rid of failure state
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        cout << "Enter option" << endl;
        cout << "1)Add\n2)Delete\n3)Same flight passengers\n4)Exit\n";
        cin >> option;
        if (option == 1)
        {
            cout << "Enter last name" << endl;
            cin >> tic.LastName;
            if (std::cin.fail())
            {
                // get rid of failure state
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            cout << "Enter passportId" << endl;
            cin >> tic.passportId;
            if (std::cin.fail())
            {
                // get rid of failure state
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            cout << "Enter flightCode" << endl;
            cin >> tic.FlightCode;
            if (std::cin.fail())
            {
                // get rid of failure state
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            cout << "Enter Seat" << endl;
            if (std::cin.fail())
            {
                // get rid of failure state
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            cin >> tic.Seat;
            cout << "Enter Priority boarding [1/0]" << endl;
            if (std::cin.fail())
            {
                // get rid of failure state
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            cin >> tic.PriorityBoarding;
            if (std::cin.fail())
            {
                // get rid of failure state
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            key = table.createKey(tic);
            cout << "Key is: " << key << endl;
            table.insertEntry(key, tic);
            cout << "Main list: " << endl;
            table.printMain();
            cout << "Second list: " << endl;
            table.printSecond();
        }
        else if (option == 2)
        {
            int new_key = 0;
            cout << "Enter key you want to delete: " << endl;
            if (std::cin.fail())
            {
                // get rid of failure state
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            cin >> new_key;
            table.removeEntry(new_key);
            table.printMain();
            table.printSecond();
        }
        else if (option == 3)
        {
            string new_key;
            cout << "Enter flight code " << endl;
            if (std::cin.fail())
            {
                // get rid of failure state
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
            cin >> new_key;
            table.printNames(new_key);
        }
        else if (option == 4)
        {
            exit(0);
        }
    }
}