#include "ticket.h"
#include <vector>
#include <cmath>


int const PRIME= 37;
int const SEED= 173;
int some_id = 232900;
int HASHTABLE_CAPACITY = 2;

struct passInfo
{
    std::string seat;
    std::string name;
};

struct Node
{
    unsigned long long int key;
    ticket ticekt_;
};

struct Second_Node
{
    std::string FlightCode;
    std::vector<passInfo> list_;
};


class hashtable
{
private:
    Node * tic;
    Second_Node * pass_;
    float loadness;
    int size;
    
    
public:
    hashtable();
    ~hashtable();

    unsigned long long int createKey(ticket inf); 
    Node createNode (unsigned long long int key, ticket inf);
    unsigned long long int getHash(unsigned long long int key);
    unsigned long long int EnCodeString(std::string key);
    void insertEntry (unsigned long long int, ticket inf); 
    void insertSecondEntry(std::string key, passInfo node);
    void removeEntry(unsigned long long int);
    void removeSecondEntry(std::string key, std::string seat);
    void printMain();
    void printSecond();
    void printNames(std::string FlightCode);
    void rehash(); 
    bool isSimple(int n);
};

hashtable::hashtable()
{
    loadness = 0;
    size = 0;
    tic = new Node [HASHTABLE_CAPACITY];
    pass_ = new Second_Node [HASHTABLE_CAPACITY];
    for (int i = 0; i < HASHTABLE_CAPACITY; i++)
    {
        tic[i].key = 0;                                 // 0 = empty; (if flightCode == "" and key isn't empty) == deleted;
        pass_[i].FlightCode = "";                       // "" = empty; 
    }
}

hashtable::~hashtable()
{
    delete[] tic;
}


Node hashtable::createNode (unsigned long long int key, ticket inf)
{
    Node nd;
    nd.key = key;
    nd.ticekt_ = inf;
    return nd;
}

unsigned long long int hashtable::getHash(unsigned long long int key)
{
    unsigned long long int coefficient = key; // усложнение ключа
    int tableSize = HASHTABLE_CAPACITY;
    int hash = coefficient % tableSize;
    return hash;
}

// int keyHashCode(int key)
// {

// }


unsigned long long int hashtable::EnCodeString(std::string key)
{
    int s = key.size();
    unsigned long long int sum = 0;
    for (int i = 0; i < key.size(); i++)
    {
        sum += key[i] * pow(27, s);
        s--;
    }
    return sum;
}

void hashtable::insertEntry (unsigned long long int key, ticket inf)
{
    Node newEntry = createNode(key, inf);
    bool tester = false;
    int hash = getHash(key);
    Node currentEntry = tic[hash];
    if (currentEntry.key == 0)
    {
        tic[hash] = newEntry;
        tic[hash].ticekt_ = newEntry.ticekt_;
        tic[hash].key = key;
        passInfo inf;
        inf.name = newEntry.ticekt_.LastName;
        inf.seat = newEntry.ticekt_.Seat;
        insertSecondEntry(newEntry.ticekt_.FlightCode, inf);
        size++;
        loadness = static_cast<float>(size) / HASHTABLE_CAPACITY;
    }
    else
    {
        if (loadness >= 1)
        {
            rehash();
        }
        for (int i = 0; tic[hash].key != 0 || (tic[hash].key != 0 && tic[hash].ticekt_.FlightCode == ""); i++)
        {
            hash = getHash(key + i);

            // tic[hash].ticekt_.FlightCode == inf.FlightCode && tic[hash].ticekt_.LastName == inf.LastName && tic[hash].ticekt_.passportId == inf.passportId && tic[hash].ticekt_.PriorityBoarding == inf.PriorityBoarding && tic[hash].ticekt_.Seat == inf.Seat
        }
        // if (tic[hash].ticekt_.FlightCode == inf.FlightCode && tic[hash].ticekt_.LastName == inf.LastName && tic[hash].ticekt_.passportId == inf.passportId && tic[hash].ticekt_.PriorityBoarding == inf.PriorityBoarding && tic[hash].ticekt_.Seat == inf.Seat)
        // {
        //     tic[hash].ticekt_ = inf;
        // }

        tic[hash] = newEntry;
        passInfo inf;
        inf.name = newEntry.ticekt_.LastName;
        inf.seat = newEntry.ticekt_.Seat;
        this->insertSecondEntry(newEntry.ticekt_.FlightCode, inf);
        size++;
        loadness = static_cast<float>(size) / HASHTABLE_CAPACITY;
    }
}

void hashtable::insertSecondEntry(std::string key, passInfo node)
{
    int second_key = EnCodeString(key);
    int hash = getHash(second_key);
    Second_Node CurrentEntry = pass_[hash];
    if (CurrentEntry.FlightCode == ""  || CurrentEntry.FlightCode == key)
    {
        pass_[hash].FlightCode = key;
        pass_[hash].list_.push_back(node);
    }
    else
    {
        for (int i = 0; pass_[hash].FlightCode != "" || (pass_[hash].FlightCode == key); i++)
        {
            hash = getHash(second_key + i);

            // tic[hash].ticekt_.FlightCode == inf.FlightCode && tic[hash].ticekt_.LastName == inf.LastName && tic[hash].ticekt_.passportId == inf.passportId && tic[hash].ticekt_.PriorityBoarding == inf.PriorityBoarding && tic[hash].ticekt_.Seat == inf.Seat
        }
        pass_[hash].FlightCode = key;
        pass_[hash].list_.push_back(node);
    }
    
}


unsigned long long int  hashtable::createKey(ticket inf)
{
    unsigned long long int key = some_id;
    some_id++;
     key += EnCodeString(inf.FlightCode);
     key += EnCodeString(inf.passportId);
     key += EnCodeString(inf.Seat);
    return key;
}


void hashtable::printMain()
{
    for (int i = 0; i < HASHTABLE_CAPACITY; i++)
    {
        if (tic[i].key != -1 && tic[i].key != 0)
        {
            std::cout << tic[i].ticekt_.LastName << "[" << i << "]" << " ";
        }
        
    }
    std::cout << std::endl;
}

void hashtable::printSecond()
{
    for (int i = 0; i < HASHTABLE_CAPACITY; i++)
    {
        if (pass_[i].FlightCode != "")
        {
            std::cout << pass_[i].FlightCode << "[" << i << "]" << " ";
        }
    }
    std::cout << std::endl;
}


void hashtable::printNames(std::string FlightCode)
{
    unsigned long long int second_key = EnCodeString(FlightCode);
    int hash = getHash(second_key);
    int pos = -1;
    for (int i = 0; i < HASHTABLE_CAPACITY; i++)
    {
        std::string str = pass_[i].FlightCode;
        if(pass_[i].FlightCode == FlightCode)
        {
            pos = i;
            break;
        }
    }
    if (pos != -1)
    {
        for (int i = 0; i < pass_[pos].list_.size(); i++)
        {
            std::cout << pass_[pos].list_[i].name << " ";
        }
        std::cout << std::endl;
    }

    std::cout << std::endl;
}


void hashtable::removeEntry(unsigned long long int key)
{
    int hash = getHash(key);
    bool tester = false;
    for (int i = 0; tic[hash].key != 0; i++)
    {
        hash = getHash(key + i);
        if (tic[hash].key == key)
        {
            tester = true;
            break;
        }
    }
    if (tester == true) 
    {
        removeSecondEntry(tic[hash].ticekt_.FlightCode, tic[hash].ticekt_.Seat);
        tic[hash].ticekt_.FlightCode = "";
        tic[hash].ticekt_.LastName = "";
        tic[hash].ticekt_.passportId = "";
        tic[hash].ticekt_.Seat = "";
        size--;
        loadness = static_cast<float>(size) / HASHTABLE_CAPACITY;
    }
}

void hashtable::removeSecondEntry(std::string key, std::string seat)
{
    unsigned long long int second_key = EnCodeString(key);
    int hash = getHash(second_key);
    for (int i = 0; i < pass_[hash].list_.size(); i++)
    {
        if (pass_[hash].list_[i].seat == seat)
        {
            if (pass_[hash].list_.size() >= 2)
            {
                for (int j = i; j < pass_[hash].list_.size() - 1; j++)
                {
                    pass_[hash].list_[j] = pass_[hash].list_[j + 1];
                }
                pass_[hash].list_.pop_back();
            }
            else
            {
                pass_[hash].list_.pop_back();
                pass_[hash].FlightCode = "";
            }
            
        }
    }
}

void hashtable::rehash()
{
    int i = HASHTABLE_CAPACITY + 1;
    for (; ;i++)
    {
        if (isSimple(i))
        {
            break;
        }
    }

    Node * dest = new Node[i];
    Node * old = tic;
    tic = dest;
    int old_capacity = HASHTABLE_CAPACITY;
    HASHTABLE_CAPACITY = i;
    loadness = 0;
    this->size = 0;
    Second_Node * second_dest = new Second_Node[HASHTABLE_CAPACITY];
    Second_Node * old_second = pass_;
    pass_ = second_dest;
    std::string st;
    for (int j = 0; j < old_capacity; j++)
    {   st = pass_[j].FlightCode;
        if (old[j].key != 0)
        {
            ticket to = old[j].ticekt_;
            int k = old[j].key;
            this->insertEntry(old[j].key, old[j].ticekt_);
        }
        st = pass_[j].FlightCode;
    }
    
    delete[] old;
    delete[] old_second;
    
    std::cout << "\nREHASH!!!" << std::endl;
    std::cout << "New capacity: " << HASHTABLE_CAPACITY << std::endl;
    std::cout << "\n";

    for (int i = 0; i < HASHTABLE_CAPACITY; i++)
    {
        if (pass_[i].FlightCode != "")
        {
            std::cout << pass_[i].FlightCode << ", " << std::endl;
        }  
    }
}

bool hashtable::isSimple(int n)
{
    if (n <= 1)
    {
        return false;
    }
    for (int i = 2; i <= sqrt(n); i++)
    {
        if (n % i == 0)
            return false;
    }

    return true;
}
